<?php

namespace RPCApi;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RPCServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

        require base_path('routes/rpc.php');
        //$this->loadRoutesFrom(base_path('routes/rpc.php'));
    }
}
