<?php

namespace RPCApi;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class RPC {

    private static $instance;

    /**
     * @var Application
     */
    protected $app;

    private function __construct(Application $app) {
        $this->app = $app;
    }

    public static function getInstance(Application $app) {
        if (!self::$instance) {
            self::$instance = new self($app);
        }

        return self::$instance;
    }

    public function process(Request $request) {

        $path = trim($request->path(), '/');
        $prefix = trim($request->route()->getPrefix());
        if ($prefix) {
            $path = explode($prefix, $path);
            array_shift($path);
            $path = implode('', $path);
        }

        $path = trim(str_replace($prefix, '', $path), '/');
        $path = explode('/', $path);
        $path = implode("\\", $path);


        $namespace = $this->app->config->get('rpc.namespace', __NAMESPACE__);

        $classPath = $namespace . "\\" . $path;
        if (!class_exists($classPath)) {
            throw new \Exception('unknown class ' . $classPath);
        }

        $classReflection = new \ReflectionClass($classPath);

        $data = [];
        if ($request->isJson()) {
            $data = $request->json()->all();
        }

        $params = $data['params'] ?? [];

        $method = $data['method'] ?? null;
        $method = ($method && is_scalar($method)) ? $method : '__invoke';

        // Не указан метод -  вызываем метод __invoke
        // Нет метода __invoke либо он !public
        if (!$classReflection->hasMethod('__invoke') || !($methodInvoke = $classReflection->getMethod($method))->isPublic()) {
            throw new \Exception('class ' . $classPath . ' is not invokable');
        }

        if ($methodInvoke->isStatic()) {
            $objectToInvoke = null;
        } else {
            // Конструируем объект
            $arguments = [];
            if ($classReflection->hasMethod('__construct')) {
                $constructor = $classReflection->getConstructor();
                $arguments = $this->fillMethodArguments($constructor);
            }

            $objectToInvoke = $classReflection->newInstanceArgs($arguments);
        }

        return $this->invokeMethod($objectToInvoke, $methodInvoke, $params);
    }

    private function invokeMethod(?object $objectToInvokeOn, \ReflectionMethod $method, ?array $params) {
        $arguments = $this->fillMethodArguments($method, $params);
        return $method->invokeArgs($objectToInvokeOn, $arguments);
    }

    private function fillMethodArguments(\ReflectionMethod $method, ?array $data = [], ?bool $useAppAsFallback = true) {

        if (!$data) {
            $data = [];
        }

        $arguments = [];

        $parameters = $method->getParameters();

        foreach ($parameters as $parameter) {
            $name = $parameter->getName();
            $position = $parameter->getPosition();
            $filled = false;

            if (array_key_exists($name, $data)) {
                $arguments[$position] = $data[$name];
                $filled = true;
            } else {
                if ($useAppAsFallback) {
                    $paramType = $parameter->getType();
                    if ($paramType) {
                        $typeName = $paramType->getName();
                        if ($this->app->has($typeName)) {
                            $arguments[$position] = $this->app->get($typeName);
                            $filled = true;
                        }
                    }
                }
            }

            if (!$filled && !$parameter->isOptional()) {
                $className = $method->getDeclaringClass()->getName();
                throw new \Exception('invalid argument ' . $name . ' in method ' . $className . '::' . $method->getName());
            }
        }

        return $arguments;
    }
}
