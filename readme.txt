1.
composer.json:
...
"repositories": [
        {
            "type": "vcs",
            "url": "https://favmel81@bitbucket.org/favmel81/composer_rpcapi.git"
        }
    ],
...

"require": {
        "favmel81/composer_rpcapi": "dev-master"
    },

2.
config/app.php:

'providers' => [
...

 \RPCApi\RPCServiceProvider::class

]

3.
routes/rpc.php:

<?php
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;


Route::prefix('rpc')->any('/{path}', function (Request $request, Application $app) {

    return \RPCApi\RPC::getInstance($app)->process($request);

})->where('path', '.*');

prefix 'rpc' matches http://domain/rpc/path

4. optional

config/rpc.php

return [
'namespace' => 'RootRPC'
];

by default target class will be find in namespace RPCApi:
http://domain/rpc/Test -> RPCApi\Test

but if exists config option rpc.namespace:
http://domain/rpc/Test -> RootRPC\Test

in this case composer.json needs some additional in autoloader section:

"autoload": {
        "psr-4": {
           ...
            "RootRPC\\": "lib/RootRPC/"
        },

"lib" is just example